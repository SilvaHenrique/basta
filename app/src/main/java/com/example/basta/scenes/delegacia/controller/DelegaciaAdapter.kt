package com.example.basta.scenes.delegacia.controller

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.basta.R
import com.example.basta.scenes.delegacia.model.Delegacia
import com.example.basta.scenes.delegacia.view.DelegaciaActivity
import com.example.basta.scenes.statistic.model.StatiscitsModel
import kotlinx.android.synthetic.main.delegacia_item.*

class DelegaciaAdapter(val context: Context, val delegaciasList: List<Delegacia>) :
    RecyclerView.Adapter<DelegaciaAdapter.DelegaciaViewHolder>() {

    class DelegaciaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        internal var text_view_delegacia : TextView
        internal var text_view_endereco_delegacia: TextView
        internal var image_view: ImageView
        init{
            text_view_delegacia = itemView.findViewById(R.id.nome_delegacia)
            text_view_endereco_delegacia = itemView.findViewById(R.id.endereco_delegacia)
            image_view = itemView.findViewById(R.id.item_image_statistics)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DelegaciaViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.delegacia_item, parent, false)
        return DelegaciaViewHolder(view)
    }

    override fun getItemCount(): Int {
        return delegaciasList.size
    }

    override fun onBindViewHolder(holder: DelegaciaViewHolder, position: Int) {
        var imageView: Int = StatiscitsModel
        holder.text_view_delegacia.text = delegaciasList[position].nameDelegacia
        holder.text_view_endereco_delegacia.text = delegaciasList[position].enderecoDelegacia
    }

}