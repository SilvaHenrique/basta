package com.example.basta.scenes.basicfunctions

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

fun AppCompatActivity.alert(text: String){
    runOnUiThread { AlartView.alartOk(this, text) }
}