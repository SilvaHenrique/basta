package com.example.basta.scenes.account.model

data class Usuario (
    var id : String? = "",
    var usuarioId: String? = "",
    var name: String = "",
    var surname: String = "",
    var document: String = "",
    var cellphone: String = "",
    var email: String = ""
)