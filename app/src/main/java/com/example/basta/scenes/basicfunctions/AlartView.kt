package com.example.basta.scenes.basicfunctions

import android.app.AlertDialog
import android.content.Context

class AlartView {
    companion object{
        fun alartOk(context: Context, message: String, title: String = "", ok: (() -> Unit)? = null){
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(message)
            builder.setPositiveButton("OK") { _, _ ->
                ok?.let { it() }
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

    }
}