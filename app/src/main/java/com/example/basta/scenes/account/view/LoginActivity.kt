package com.example.basta.scenes.account.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import com.example.basta.R
import com.example.basta.scenes.account.controller.LoginController
import com.example.basta.scenes.basicfunctions.AlartView
import com.example.basta.scenes.menu.view.MenuActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    private val controller = LoginController(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        addEvents()
    }

    private fun loginFirebase() {

    }

    fun addEvents(){

        enter_login_button.setOnClickListener {
            val email = edit_text_login_email.text.toString()
            val password = edit_text_passowrd_login.text.toString()
            this.controller.login(email, password, {
            }, { mensagem -> "usuario nao encontrado"
            })
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        cria_conta_button.setOnClickListener {
            Log.d("LoginActivity", "Entra na creat")
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }
}