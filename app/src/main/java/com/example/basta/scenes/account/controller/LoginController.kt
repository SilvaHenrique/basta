package com.example.basta.scenes.account.controller

import android.util.Log
import android.widget.Toast
import com.example.basta.scenes.account.model.Usuario
import com.example.basta.scenes.account.view.LoginActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_register.*

class LoginController(val context: LoginActivity) {

    var db = FirebaseFirestore.getInstance()

    fun login(email: String, password: String, success: ((Usuario) -> Unit), failure: ((String) -> Unit)) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnCompleteListener {
            if (it.isSuccessful) {
                it.result?.let { result ->
                    db.collection("usuario")
                        .whereEqualTo("usuarioId", result.user!!.uid).get()
                        .addOnCompleteListener {snapshot ->
                        if (snapshot.isSuccessful) {
                            snapshot.result!!.documents.firstOrNull()?.let { document ->
                                document.toObject(Usuario::class.java)?.let { usuario ->
                                     success.invoke(usuario)
                                }
                            } ?: failure.invoke("Usuario nao encontrado");
                        } else {
                            failure.invoke("Usuario nao encontrado")
                        }
                    }
                }
            } else {
                it.exception?.message?.let {
                    failure.invoke(it)
                }
            }
        }
    }
}