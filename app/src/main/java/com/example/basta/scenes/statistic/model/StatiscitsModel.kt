package com.example.basta.scenes.statistic.model

data class StatiscitsModel(
    var titleStatistics: String = "",
    var detailsStatistics: String = "",
    var image: Int  )