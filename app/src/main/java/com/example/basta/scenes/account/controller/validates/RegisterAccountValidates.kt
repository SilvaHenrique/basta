package com.example.basta.scenes.account.controller.validates

import com.example.basta.scenes.account.controller.RegisterAccountInteractor
import com.example.basta.scenes.account.view.RegisterActivity
import com.example.basta.scenes.basicfunctions.alert

class RegisterAccountValidates(val context: RegisterActivity) {

    val interactor = RegisterAccountInteractor(this.context)

    fun create(name: String?, surname: String?, document: String?, cellphone: String?, email: String?, password:String? ){
        if(!this.verifyFailed(name, surname,  document, cellphone, email,  password)){
            return
        }
        this.interactor.create(name, surname, document, cellphone, email, password)
    }

    private fun verifyFailed(name: String?, surname: String?, document: String?, cellphone: String?, email: String?, password: String?): Boolean {
        name?.let {
            if (it.isEmpty()){
                this.context.alert("Informe seu nome")
                return false
            }
        }
        surname?.let {
            if(it.isEmpty()){
                this.context.alert("Informe seu sobrenome")
                return false
            }
        }
        document?.let {
            if(it.count() < 11){
                this.context.alert("O CPF deve ser valido")
                return false
            }
        }
        cellphone?.let {
            if (it.count() != 11) {
                this.context.alert("Informe um celular válido, com DDD")
                return false
            }
        }

        email?.let {
            if(it.isEmpty()){
                this.context.alert("Informe um email válido \nLembre-se que o e-mail será usado para fazer login")
                return false
            }
        }
        password?.let{
            if(it.count() <= 5){
                this.context.alert("A senha deve conter no minímo 6 digitos")
                return false
            }
        }

        return true
    }
}