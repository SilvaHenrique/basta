package com.example.basta.scenes.account.controller

import android.content.Intent
import com.example.basta.scenes.account.firebase.RegisterFirebase
import com.example.basta.scenes.account.model.Usuario
import com.example.basta.scenes.account.view.RegisterActivity
import com.example.basta.scenes.basicfunctions.alert
import com.example.basta.scenes.menu.view.MenuActivity

class RegisterAccountInteractor(val context: RegisterActivity) {

    private val usuarioRegister = RegisterFirebase(this.context)

    fun create(name: String?, surname: String?, document: String?, cellphone: String?, email: String?, password: String?) {
        val user = Usuario()
        user.name = name!!
        user.surname = surname!!
        user.document = document!!
        user.cellphone = cellphone!!
        user.email = email!!
        this.usuarioRegister.add(user, password!!,{
            val intent = Intent(this.context, MenuActivity::class.java)
            this.context.startActivity(intent)
        }, {
            this.context.alert(it)
        })
    }
}