package com.example.basta.scenes.account.firebase

import com.example.basta.scenes.account.model.Usuario
import com.example.basta.scenes.account.view.RegisterActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class RegisterFirebase(val context: RegisterActivity): UserFirebase("usuario"){

    private val auth = FirebaseAuth.getInstance()

    fun register(success: (Usuario) -> Unit, failure: (String) -> Unit){
        this.collection.whereEqualTo("email", this.auth.currentUser!!.email!!.toLowerCase()).get().addOnSuccessListener {
            it.documents.firstOrNull()?.toObject(Usuario::class.java)?.let {user ->
                user.id = it.documents.first().id
                success(user)
            } ?:failure("Usuário não encontrado")
        }.addOnFailureListener {
            it.message?.let(failure)
        }
    }

//    fun register(user: Usuario, password: String, success: (() -> Unit), failure: () -> Unit) {
//        auth.createUserWithEmailAndPassword(user.email.toLowerCase(), password).addOnCompleteListener {
//            if(it.isSuccessful){
//                it.result?.let{
//                    success.invoke()
//                    it.user?.uid
//
//                }
//            }else{
//
//            }
//        }
//    }

    fun add(user: Usuario, password: String, success: (Usuario) -> Unit, failure: (String) -> Unit) {
        this.auth.createUserWithEmailAndPassword(user.email.toLowerCase(), password).addOnSuccessListener {
            user.usuarioId = it.user!!.uid
            this.collection.add(user).addOnSuccessListener {
                this.register(success, failure)
            }.addOnFailureListener{
                it.message?.let(failure)
            }
        }.addOnFailureListener {
            it.message?.let(failure)
        }
    }

}

