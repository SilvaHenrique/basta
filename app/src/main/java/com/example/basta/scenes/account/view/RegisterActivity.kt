package com.example.basta.scenes.account.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.basta.R
import com.example.basta.scenes.account.firebase.RegisterFirebase
import com.example.basta.scenes.account.controller.validates.RegisterAccountValidates
import com.example.basta.scenes.account.model.Usuario
import com.example.basta.scenes.menu.view.MenuActivity
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

   //private val controller = RegisterController()
   private val presenter = RegisterAccountValidates(this)
    private val usuarioRegister = RegisterFirebase(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        addEventes()
    }

    private fun addEventes() {

        profile_image.setOnClickListener {
            Log.d("RegisterActivity", "estou selecionando a foto")
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }

        register_conta_button.setOnClickListener {
            val name = edit_text_name.text.toString()
            val surname = edit_text_sur_name.text.toString()
            val documents = edit_text_cpf.text.toString()
            val cellphone = edit_text_phone.text.toString()
            val email = edit_text_create_email.text.toString()
            val password = edit_text_create_passowrd.text.toString()
           this.presenter.create(name, surname, documents, cellphone, email,  password)
        }

        termo_uso_cadastrar.setOnClickListener {
            Log.d("RegisterActivity", "Entrando na fragment")
            val intent = Intent(this, TermoDeUsoActivity::class.java)
            startActivity(intent)
        }


    }




}
