package com.example.basta.scenes.menu.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.basta.R
import com.example.basta.scenes.account.view.ProfileActivity
import com.example.basta.scenes.delegacia.view.DelegaciaActivity
import com.example.basta.scenes.statistic.view.StatisticActivity
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        addEvents()
    }

    private fun addEvents() {
        button_delegacia.setOnClickListener {
            val intent = Intent(this, DelegaciaActivity::class.java)
            startActivity(intent)
        }

        button_profile.setOnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        }
        dados_estatisticas.setOnClickListener {
            val intent = Intent(this, StatisticActivity::class.java)
            startActivity(intent)
        }

    }
}
