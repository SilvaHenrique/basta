package com.example.basta.scenes.delegacia.model

data class Delegacia (
    var nameDelegacia: String= "",
    var enderecoDelegacia: String = ""
)
