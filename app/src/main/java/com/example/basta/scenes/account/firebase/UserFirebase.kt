package com.example.basta.scenes.account.firebase

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings

open class UserFirebase(collectionName: String) {

    internal val collection: CollectionReference by lazy {
        this.db.collection(collectionName)
    }

    private val db: FirebaseFirestore by lazy {
        val db = FirebaseFirestore.getInstance()
        val setting = FirebaseFirestoreSettings.Builder()
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        db.firestoreSettings = setting
        db
    }

}