package com.example.basta.scenes.statistic.controller

import android.content.Context
import android.media.Image
import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.basta.R
import com.example.basta.scenes.statistic.model.StatiscitsModel

class StatisticsAdapter(val context: Context, val statisticsList: List<StatiscitsModel>) :
    RecyclerView.Adapter<StatisticsAdapter.StatisticsViewHolder>() {

    class StatisticsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        internal var text_view_title : TextView
        internal var text_view_details: TextView
        internal var image: Image

        init {
            text_view_title = itemView.findViewById(R.id.text_view_title_statistics)
            text_view_details = itemView.findViewById(R.id.text_view_details)
            image = itemView.findViewById(R.id.item_image_statistics)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatisticsViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.statistics_item, parent, false)
        return StatisticsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return statisticsList.size
    }

    override fun onBindViewHolder(holder: StatisticsViewHolder, position: Int) {
        holder.text_view_title.text = statisticsList[position].titleStatistics
        holder.text_view_details.text = statisticsList[position].detailsStatistics

    }
}