package com.example.basta.scenes.delegacia.view

import android.graphics.drawable.GradientDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.basta.R
import com.example.basta.scenes.delegacia.controller.DelegaciaAdapter
import com.example.basta.scenes.delegacia.model.Delegacia
import kotlinx.android.synthetic.main.activity_delegacia.*
import kotlinx.android.synthetic.main.delegacia_item.*

class DelegaciaActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delegacia)

        listConfigure()

    }

    fun delegacias (): List<Delegacia>{
        return listOf(
            Delegacia("Delegacia da mulher","Av. Corifeu de Azevedo Marques, 4300 - Vila Lageado, São Paulo - SP, 05339-002"),
            Delegacia("Delegacias de Defesa da Mulher e DPs", "Av. 11 de Junho, 89 - Vila Clementino, São Paulo - SP, 04041-050"),
            Delegacia("Delegacia da mulher","Av. Corifeu de Azevedo Marques, 4300 - Vila Lageado, São Paulo - SP, 05339-002"),
            Delegacia("Delegacia da mulher","Av. Corifeu de Azevedo Marques, 4300 - Vila Lageado, São Paulo - SP, 05339-002"),
            Delegacia("Delegacia da mulher","Av. Corifeu de Azevedo Marques, 4300 - Vila Lageado, São Paulo - SP, 05339-002"),
            Delegacia("Delegacia da mulher","Av. Corifeu de Azevedo Marques, 4300 - Vila Lageado, São Paulo - SP, 05339-002"),
            Delegacia("Delegacia da mulher","Av. Corifeu de Azevedo Marques, 4300 - Vila Lageado, São Paulo - SP, 05339-002"),
            Delegacia("Delegacia da mulher","Av. Corifeu de Azevedo Marques, 4300 - Vila Lageado, São Paulo - SP, 05339-002"),
            Delegacia("Delegacias de Defesa da Mulher e DPs", "Av. 11 de Junho, 89 - Vila Clementino, São Paulo - SP, 04041-050")
        )
    }

    fun listConfigure(){
        val recyclerView = recycle_view_delegacia
        recyclerView.adapter = DelegaciaAdapter(this, delegacias())
        val layoutManager = StaggeredGridLayoutManager(
            2, StaggeredGridLayoutManager.VERTICAL)
        recyclerView.layoutManager = layoutManager
    }


}
